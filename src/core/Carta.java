package core;

public interface Carta{
	
	public String numero = "";
	public String palo = "";
	public String getNumero();
	public void setNumero(String numero);
	public String getPalo();
	public void setPalo(String palo);
	public String toString();
	
}
